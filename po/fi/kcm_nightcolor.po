# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2017, 2019, 2021, 2022.
# Lasse Liehu <lasse.liehu@iki.fi>, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 01:39+0000\n"
"PO-Revision-Date: 2022-11-11 22:21+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Napauta sijaintiasi kartalla."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Napsauta sijaintiasi kartalla."

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "Lähennä"

#: ui/LocationsFixedView.qml:222
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Muokattu <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>maailman sijaintikartasta</"
"link>, TUBS / Wikimedia Commons / <link url='https://creativecommons.org/"
"licenses/by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:235
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Leveysaste:"

#: ui/LocationsFixedView.qml:261
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Pituusaste:"

#: ui/main.qml:102
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "Sinisen valon suodatin tekee näyttöväreistä lämpimämmät."

#: ui/main.qml:113
#, kde-format
msgid "Switching times:"
msgstr "Vaihtoajat:"

#: ui/main.qml:115
#, kde-format
msgid "Always off"
msgstr "Aina pois käytöstä"

#: ui/main.qml:116
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Auringonlasku ja -nousu nykyisessä sijainnissa"

#: ui/main.qml:117
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Auringonlaskusta ja -nousu asetetussa sijainnissa"

#: ui/main.qml:118
#, kde-format
msgid "Custom times"
msgstr "Aikojen mukautus"

#: ui/main.qml:119
#, fuzzy, kde-format
#| msgid "Always on night color"
msgid "Always on night light"
msgstr "Yöväri aina käytössä"

#: ui/main.qml:171
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Laitteen sijainti päivitetään säännöllisesti GPS:n avulla (jos "
"käytettävissä) tai lähettämällä verkkotiedot <a href=\"https://location."
"services.mozilla.com/\">Mozillan paikannuspalveluun</a>."

#: ui/main.qml:190
#, fuzzy, kde-format
#| msgid "Day color temperature:"
msgid "Day light temperature:"
msgstr "Päivävärin lämpötila:"

#: ui/main.qml:233 ui/main.qml:292
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1 K"

#: ui/main.qml:237 ui/main.qml:296
#, fuzzy, kde-format
#| msgctxt "No blue light filter activated"
#| msgid "Cool (no filter)"
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Kylmä (ei suodatinta)"

#: ui/main.qml:243 ui/main.qml:302
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Lämmin"

#: ui/main.qml:249
#, fuzzy, kde-format
#| msgid "Night color temperature:"
msgid "Night light temperature:"
msgstr "Yövärin lämpötila:"

#: ui/main.qml:313
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Leveysaste: %1°  Pituusaste: %2°"

#: ui/main.qml:323
#, fuzzy, kde-format
#| msgid "Begin night color at:"
msgid "Begin night light at:"
msgstr "Yövärin aloitusaika:"

#: ui/main.qml:336 ui/main.qml:359
#, kde-format
msgid "Input format: HH:MM"
msgstr "Syötemuoto: TT:MM"

#: ui/main.qml:346
#, fuzzy, kde-format
#| msgid "Begin day color at:"
msgid "Begin day light at:"
msgstr "Päivävärin aloitusaika:"

# Ellei transition ole astronomisessa kielessä vakiintunut muuksi kuin ”siirtymäksi”?
#: ui/main.qml:368
#, kde-format
msgid "Transition duration:"
msgstr "Siirtymän kesto:"

#: ui/main.qml:377
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuutti"
msgstr[1] "%1 minuuttia"

#: ui/main.qml:390
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Syöte minuutteina – väh. 1, enint. 600"

#: ui/main.qml:408
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Virhe: Siirtymäajat lomittuvat."

#: ui/main.qml:430
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Paikannetaan…"

#: ui/TimingsView.qml:32
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Värilämpötila alkaa vaihtua %1 ja on täysin vaihtunut yöväriksi %2 mennessä"

#: ui/TimingsView.qml:39
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Värilämpötila alkaa vaihtua %1 ja on täysin vaihtunut päiväväriksi %2 "
"mennessä"

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Kylmä"

#, fuzzy
#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "Tältä yöväri näyttää aktivoituna."

#, fuzzy
#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "Tältä yöväri näyttää aktivoituna."

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Aktivoi yöväri"

#~ msgid "Turn on at:"
#~ msgstr "Ota käyttöön:"

#~ msgid "Turn off at:"
#~ msgstr "Poista käytöstä:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "Yöväri alkaa vaihtua takaisin %1 ja päättyy %2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Virhe: Aamu on ennen iltaa."

#~ msgid "Detect Location"
#~ msgstr "Tunnista sijainti"

#~ msgid ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."
#~ msgstr ""
#~ "Laitteen sijainti tunnistetaan GPS:stä (jos käytettävissä) tai "
#~ "lähettämällä verkkotiedot <a href=\"https://location.services.mozilla.com"
#~ "\">Mozilla paikannuspalveluun</a>."

#~ msgid "Night Color begins at %1"
#~ msgstr "Yöväritys alkaa %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "Väri muutettu täysin %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "%1 palautti tavallisen värityksen"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Tommi Nieminen"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "translator@legisign.org"

#~ msgid "Night Color"
#~ msgstr "Yöväri"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr " K"

#~ msgid "Operation mode:"
#~ msgstr "Toimintatila:"

#~ msgid "Automatic"
#~ msgstr "Automaattinen"

#~ msgid "Times"
#~ msgstr "Ajat"

#~ msgid "Constant"
#~ msgstr "Vakio"

#~ msgid "Sunrise begins:"
#~ msgstr "Auringonnousu alkaa:"

#~ msgid "(Input format: HH:MM)"
#~ msgstr "(Syötemuoto: TT:MM)"

#~ msgid "Sunset begins:"
#~ msgstr "Auringonlasku alkaa:"

#~ msgid "...and ends:"
#~ msgstr "…ja päättyy:"

#~ msgid "Manual"
#~ msgstr "Käyttäjän asettama"

#~ msgid "(HH:MM)"
#~ msgstr "(TT:MM)"
